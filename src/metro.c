#include "metro.h"
#include "tas_vertex.h"
#include "list_vertex.h"
#include <float.h>
#include <stdio.h>
#include <string.h>

int metro(graph_t graph,int* numdepart,int* numarrivee){

  char* depart=calloc(511,sizeof(char));
  char* arrivee=calloc(511,sizeof(char));

  getchar();

  printf("Choisir le nom de la station de départ:");
  fgets(depart,511,stdin);
  if (depart[strlen(depart)-1]<32) depart[strlen(depart)-1]=0;
  printf("\n");

  printf("Choisir le nom de la station d'arrivée:");
  fgets(arrivee,511,stdin);
  if (arrivee[strlen(arrivee)-1]<32) arrivee[strlen(arrivee)-1]=0;
  printf("\n");

  // printf("Départ: $$$%s$$$\n",depart);
  // printf("Arrivée: $$$%s$$$\n",arrivee);
  // printf("Un sommet du graphe: $$$%s$$$\n",graph.data[0].nom);


//Création des liste de vertex depart (resp. arrivee) contenant tous les sommets ayant pour nom depart (resp. arrivee)
  list_vertex_t listenomdepart=list_vertex_new();
  list_vertex_t listenomarrivee=list_vertex_new();

  int i;
  for(i=0;i<graph.size_vertices;i++){
    if(strcmp(graph.data[i].nom,depart)==0){
      listenomdepart=list_vertex_add_first(&graph.data[i],listenomdepart);
    }
    else{
    }
    if(strcmp(graph.data[i].nom,arrivee)==0){
      listenomarrivee=list_vertex_add_first(&graph.data[i],listenomarrivee);
    }
    else{
    }
  }


//Ajout d'arcs fictif de cout nul au graph entre tous les sommets ayant pour nom depart et arrivee
list_vertex_t p1,p2;
p1=listenomdepart;
  while(!list_vertex_is_empty(p1)){
    p2=listenomdepart;
    while(!list_vertex_is_empty(p2)){
      if(p1!=p2){
        graph.size_edges++;
        edge_t e=arc_new(p2->val->numero,0);
        *p1->val=vertex_add_edge(*p1->val,e);
      }
      p2=p2->next;
    }
    p1=p1->next;
  }

p1=listenomarrivee;
  while(!list_vertex_is_empty(p1)){
    p2=listenomarrivee;
    while(!list_vertex_is_empty(p2)){
      if(p1!=p2){
        graph.size_edges++;
        edge_t e=arc_new(p2->val->numero,0);
        *p1->val=vertex_add_edge(*p1->val,e);
      }
      p2=p2->next;
    }
    p1=p1->next;
  }


  if((list_vertex_is_empty(listenomdepart)) || (list_vertex_is_empty(listenomarrivee))){
    printf("Départ ou arrivée non reconnue\n");
    free(depart);
    free(arrivee);
    return(0);
  }


//Définir numdepart et numarrivee
  *numdepart=listenomdepart->val->numero;
  *numarrivee=listenomarrivee->val->numero;

  list_vertex_delete(listenomdepart);
  list_vertex_delete(listenomarrivee);
  free(depart);
  free(arrivee);

//A RAJOUTER SI ON VEUT VERIFIER L'APPARTENANCE DES SOMMETS ARRIVEE ET DEPART AU graph
  // if (graph_find_vertex(graph, *numdepart) == 0){
  //   fprintf( stderr, "DEPART IS NOT IN GRAPH\n" );
  //   return 0;
  // }
  // if (graph_find_vertex(graph, arrivee) == 0){
  //   fprintf( stderr, "ARRIVEE IS NOT IN GRAPH\n" );
  //   return 0;
  // }
  //



  tas_vertex_t Atraiter = tas_vertex_new(graph.size_edges);
  tas_vertex_t Atteint = tas_vertex_new(graph.size_vertices);
  vertex_t* u;
  double disttemp;

  // INITIALISATION DES COUTS A L'INFINI DEFINI PAR DBL_MAX
  for (i=0; i<graph.size_vertices; i++){
      graph.data[i].pcc = DBL_MAX;
      graph.data[i].cout = DBL_MAX;
  }


  graph.data[*numdepart].pcc = 0;
  graph.data[*numdepart].cout = 0;



  // ON AJOUTE LE SOMMET DE DEPART DANS ATRAITER
  Atraiter = tas_vertex_add(&graph.data[*numdepart],&Atraiter);

  //TANT QUE ATRAITER N'EST PAS VIDE
  while (!tas_vertex_is_empty(Atraiter)){


    //recherche du sommet de plus faible cout dans ATRAITER et suppression de ce sommet

    u=tas_vertex_get_min(Atraiter);
    tas_vertex_delete_min(&Atraiter);

    if (u->numero == *numarrivee){
      tas_vertex_delete(Atteint);
      tas_vertex_delete(Atraiter);
      //solution trouvée
      return 1;
    }
    //Sinon on a pas trouvé la solution mais le plus court chemin de départ à u
    else{

      //on ajoute ce sommet dans la liste des sommets atteints
      Atteint = tas_vertex_add(u,&Atteint);

      //boucle sur tous les sommets voisins de u

      listedge_t pointeur=u->edges;

        while(!(listedge_is_empty(pointeur))){

          disttemp=u->pcc + pointeur->val.cout;
          vertex_t* v = &graph.data[pointeur->val.arrivee];

          if ((tas_vertex_find(v,Atteint)==0) && (disttemp < v->pcc)){

            v->cout = disttemp+vertex_heuristique(*v,graph.data[*numarrivee]);
            v->pcc = disttemp;
            //Ajout du père dans le liste_vertex graph_find_vertex
            v->pere=u->numero;
            //et on ajoute le sommet v dans atraiter
            Atraiter = tas_vertex_add(v, &Atraiter);


          }
          pointeur=pointeur->next;
        }
    }
  }

  tas_vertex_delete(Atraiter);
  tas_vertex_delete(Atteint);


  if (graph.data[graph_find_vertex(graph,*numarrivee)].cout != DBL_MAX){
    return 1;
  }
  return 0;
}
