#include "graph.h"

graph_t graph_init(int size_v, int size_e){
  graph_t graph;

  graph.nverts = 0; 
  graph.size_vertices=size_v;
  graph.size_edges=size_e;
//  graph.data=calloc(size_v,sizeof(vertex_t));
  graph.data=calloc(graph.size_vertices,sizeof(graph.data[0]));
  return(graph);
}

graph_t graph_add_vertex(graph_t graph, vertex_t s){

  if ( graph.nverts == graph.size_vertices ) {
    fprintf( stderr, "NOT ADDING NEW VERTEX (LIMIT REACHED)\n" );
    return graph;
  }

  graph.data[graph.nverts]=s;
  graph.nverts++;
  return(graph);
}

int graph_find_vertex(graph_t graph, int numero){
  int i;
  for(i=0;i<graph.size_vertices;i++){
    if(numero==graph.data[i].numero){
      return(i);
    }
  }
  return(-1);
}

void graph_print(graph_t graph){
  printf( "****************************************\n" );
  printf("Données du graphe:\n\n");
  printf("  Nombre de sommets: %d\n  Nombre total d'arc: %d\n\n",graph.size_vertices,graph.size_edges);
  printf("  Liste des différentes sommets contenu dans le graph:\n");
  int i;
  for (i=0;i<graph.size_vertices;i++){
    printf("  ");
    vertex_print(graph.data[i]);
  }
  printf("****************************************\n");
}

void graph_del(graph_t graph){
  int i;
  for(i=0;i<graph.nverts;i++){
    graph.data[i].edges=listedge_delete(graph.data[i].edges);
    free(graph.data[i].ligne);
    free(graph.data[i].nom);
  }
  free(graph.data);
}
