#include "listedges.h"



// pour plus de propreté on définit NIL (identifiant de fin de liste) comme NULL
#define NIL NULL

listedge_t listedge_new(){
  return NIL;
}

int listedge_is_empty(listedge_t l){
  return NIL == l;
}

void listedge_print(listedge_t l) {
  listedge_t p;
  printf("( ");
  for ( p=l; ! listedge_is_empty(p); p = p->next) {
    arc_print( p->val );
    printf( " " );
  }
  printf(")\n");
}


listedge_t listedge_find(int numerosommet, listedge_t l){
  listedge_t p;
  for( p=l; ! listedge_is_empty(p) ; p=p->next ) {
    if(p->val.arrivee == numerosommet) {
      return p;
    }
  }
  return NULL;
}

listedge_t listedge_find_double(int numerosommet, double cout, listedge_t l){
  listedge_t p;
  for( p=l; ! listedge_is_empty(p) ; p=p->next ) {
    if((p->val.arrivee == numerosommet) && (p->val.cout==cout)) {
      return p;
    }
  }
  return NULL;
}




int listedge_length(listedge_t l){
  int len = 0;
  listedge_t p;
  for( p=l; ! listedge_is_empty(p) ; p=p->next ) {
    len ++;
  }
  return len;
}

listedge_t listedge_del_first(listedge_t l){
  assert(!listedge_is_empty(l));
  listedge_t p = l->next;
  free( l );
  return p;
}

listedge_t listedge_add_first(edge_t e, listedge_t l){
  listedge_t p = calloc( 1, sizeof( *p ) );
  if ( NULL == p ) {
    fprintf( stderr, "Fatal: Unable to allocate new list link.\n" );
    return l;
  }
  p->val  = e;
  p->next = l;
  return p;
}


listedge_t listedge_add_last(edge_t e, listedge_t l){
  listedge_t p2 = l;
  listedge_t p = calloc( 1, sizeof( *p ) );
  if ( NULL == p ) {
    fprintf( stderr, "Fatal: Unable to allocate new list link.\n" );
    return l;
  }
  p->val = e;
  p->next = NIL;
  if (listedge_is_empty(p2)){
    return p;
  }

  while (!listedge_is_empty(p2->next)){
    p2 = p2->next;
  }
  p2->next = p;
  return l;
}

listedge_t listedge_copy(listedge_t l){
  if (listedge_is_empty(l)){
    return NIL;
  }
  listedge_t p = l;
  listedge_t copy = listedge_new();

  while (!listedge_is_empty(p)){
    copy = listedge_add_last(p->val,copy);
    p = p->next;
  }
  return copy;
}

listedge_t listedge_delete(listedge_t l){
  listedge_t p=l;
  while (!listedge_is_empty(p)){
    p = listedge_del_first(p);
  }
  return listedge_new();
}
