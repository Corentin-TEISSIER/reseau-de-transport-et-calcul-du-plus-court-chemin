#include "readgraph.h"


graph_t readgraph(char* nom,graph_t* graph){

  FILE* f;
  int numero,nbsommet,nbarc,compteur1 = 0, compteur2 = 0, sommetdep, sommetarr;
  double lat,longi ;
  // char* line=malloc(sizeof(*line)*128);
  // char* mot=malloc(sizeof(*mot)*512);
  char line[128];
  char mot[512];
  double valeur;
  //graph_t graph;

  f=fopen(nom,"r");
  if (f==NULL) { printf("Impossible d’ouvrir le fichier\n"); exit(EXIT_FAILURE);}
  /* Lecture de la premiere ligne du fichier : nombre de sommets et nombre d’arcs */
  fscanf(f,"%d %d ",&nbsommet,&nbarc);
  *graph=graph_init(nbsommet,nbarc);
  /* Ligne de texte "Sommets du graphe" qui ne sert a rien */
  fgets(mot,511,f);

  while(compteur1 != nbsommet){
    /* lecture d’une ligne de description d’un sommet */
    /* on lit d’abord le numero du sommet, sa position et le nom de la ligne */
    fscanf(f,"%d %lf %lf %s", &numero, &lat, &longi, line);
    /* numero contient alors l’entier ou numero du sommet, lat et longi la position, line le nom de la
    ligne */
    vertex_t sommet;
    sommet.numero=numero;
    sommet.x=lat;
    sommet.y=longi;
    sommet.ligne=strdup( line );
    sommet.cout=0;
    /* Le nom de la station peut contenir des separateurs comme l’espace. On utilise plutot fgets pour lire
    toute la fin de ligne */
    fgets(mot,511,f);
    /* fgets a lu toute la ligne, y compris le retour a la ligne. On supprime le caractere ’\n’ qui peut se
    trouver a la fin de la chaine mot : */
    if (mot[strlen(mot)-1]<32) mot[strlen(mot)-1]=0;
    if(mot[0]==9){
      char tampon[511];
      int i;
      for(i=0;i<510;i++){
        tampon[i]=mot[i+1];
      }
      tampon[510]=0;
      for(i=0;i<510;i++){
        mot[i]=tampon[i];
      }
    }
    /* mot contient desormais le nom du sommet, espaces eventuels inclus. */
    sommet.nom=strdup( mot );
    sommet.edges = listedge_new();
    *graph=graph_add_vertex(*graph,sommet);
    /* graph contient desormais le sommet*/
    compteur1++;
  }

  /*Pour sauter les lignes de commentaires, on peut simplement utiliser la fonction fgets, sans exploiter
  la chaine de caracteres lue dans le fichier */
  fgets(mot,511,f);

  while(compteur2 != nbarc){
    /* on lit l'arc et sa valeur */
    fscanf(f,"%d %d %lf", &sommetdep, &sommetarr, &valeur);
    int indice=graph_find_vertex(*graph,sommetdep);
    if(indice!=-1){
      edge_t e = arc_new (sommetarr,valeur);
      graph->data[indice]=vertex_add_edge(graph->data[indice], e );
    }
    compteur2++;
    /* on s'assure d'arriver à la fin de la ligne au cas où */
    fgets(mot,511,f);
  }
  /* Ne pas oublier de fermer votre fichier */
  fclose(f);
  return(*graph);
}
