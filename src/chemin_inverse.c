#include <stdio.h>
#include "chemin_inverse.h"

list_vertex_t chemin_inverse(graph_t graph, int depart, int arrivee, list_vertex_t* listpere){
  if(strcmp(graph.data[arrivee].nom,graph.data[depart].nom)==0){
    return *listpere;
  }
  else{
    *listpere=list_vertex_add_first(&graph.data[graph.data[arrivee].pere],*listpere);
    return(chemin_inverse(graph, depart, (*listpere)->val->numero, listpere));
  }
}

list_vertex_t chemin_inverse_del_double_arrivee(list_vertex_t listpere){
  list_vertex_t pointeur=listpere;
  while(!list_vertex_is_empty(pointeur->next)){

    listedge_t pointedge=listedge_new();
    pointedge=listedge_find_double(pointeur->next->val->numero,0,pointeur->val->edges);
    edge_t arc=arc_new(pointeur->next->val->numero,0);

    if((pointedge!=NULL) && (strcmp(pointeur->val->nom,pointeur->next->val->nom)==0) && (arc_compare(pointedge->val,arc)==1)){
      list_vertex_t tampon;
      tampon=pointeur->next;
      pointeur->next=NULL;
      list_vertex_delete(tampon);
      return(listpere);
    }
    pointeur=pointeur->next;
  }
  return(listpere);
}
