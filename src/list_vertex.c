#include "list_vertex.h"

list_vertex_t list_vertex_new() {
  return NULL;
}

int list_vertex_is_empty( list_vertex_t l ) {
  return NULL == l;
}

void list_vertex_print(list_vertex_t l){
  if(l==NULL){
    printf("(//NULL//)");
  }
  list_vertex_t pointeur=l;
  printf("(");
  while(!list_vertex_is_empty(pointeur)){
    vertex_print(*(pointeur->val));
    pointeur=pointeur->next;
    printf("\n;\n");
  }
  printf(")\n\n");
}

void list_vertex_line_print(list_vertex_t l){
  if(l==NULL){
    printf("(//NULL//)");
  }
  list_vertex_t pointeur=l;
  list_vertex_t precedent=list_vertex_new();
  printf("(");
  while(!list_vertex_is_empty(pointeur)){
    vertex_print(*(pointeur->val));
    precedent=pointeur;
    pointeur=pointeur->next;
    if(list_vertex_length(pointeur)>1){
      if((strcmp(pointeur->val->nom, precedent->val->nom) == 0)&&(strcmp(pointeur->val->ligne, precedent->val->ligne)!=0)){
        printf("ATTENTION CORRESPONDANCE à %s entre la ligne %s et la ligne %s\n\n",precedent->val->nom,precedent->val->ligne,pointeur->val->ligne);
      }
    }
    if(list_vertex_length(pointeur)>1){
      if((strcmp(pointeur->val->nom, precedent->val->nom) != 0)&&(strcmp(pointeur->val->ligne, precedent->val->ligne)!=0)){
        printf("ATTENTION sortir à la station %s et marcher jusqu'a la station %s pour prendre la ligne %s\n\n",precedent->val->nom,precedent->next->val->nom, precedent->next->val->ligne);
      }
    printf("\n;\n");
    }
  }
  printf(")\n\n");
}


// Precondition : liste non vide
vertex_t* list_vertex_first(list_vertex_t l){
  assert(!list_vertex_is_empty(l));
  return l->val;
}

list_vertex_t list_vertex_add_first( vertex_t* e, list_vertex_t l ) {
  list_vertex_t p = calloc( 1, sizeof( *p ) );
  if (p==NULL) {
    fprintf( stderr, "Fatal: Unable to allocate new list link.\n" );
    return l;
  }
  p->val  = e;
  p->next = l;
  return p;
}

// Precondition : liste non vide
list_vertex_t list_vertex_del_first( list_vertex_t l ) {
  if(list_vertex_is_empty(l)){
    return(NULL);
  }
  list_vertex_t p = l->next;
  free(l);
  return p;
}

list_vertex_t list_vertex_del_last(list_vertex_t l){
  list_vertex_t inverse;
  list_vertex_t pointeur=l;
  while(!list_vertex_is_empty(pointeur)){
    inverse=list_vertex_add_first(pointeur->val,inverse);
    pointeur=pointeur->next;
    l=list_vertex_del_first(l);
  }
  inverse=list_vertex_del_first(inverse);
  pointeur=inverse;
  while(!list_vertex_is_empty(pointeur)){
    l=list_vertex_add_first(pointeur->val,l);
    pointeur=pointeur->next;
    inverse=list_vertex_del_first(inverse);
  }
  inverse=list_vertex_delete(inverse);
  return(l);
}


int list_vertex_length(list_vertex_t l) {
  int len = 0;
  list_vertex_t p;
  for( p=l; ! list_vertex_is_empty(p) ; p=p->next ) {
    len ++;
  }
  return len;
}

list_vertex_t list_vertex_find(int num, list_vertex_t l) {
  list_vertex_t p=l;
  while(!list_vertex_is_empty(p)){
    if(p->val->numero==num){
      return(p);
    }
    p=p->next;
  }
  return NULL;
}


int list_vertex_count(vertex_t e, list_vertex_t l) {
  list_vertex_t
  l1=list_vertex_new();
  l1=l;
  int counter=0;
  while (!list_vertex_is_empty(l1)){
    if (e.numero==l1->val->numero){
      counter++;
      l=l1->next;
    }
    l1=l1->next;
  }
  return(counter);
}

list_vertex_t list_vertex_add_last(vertex_t* e, list_vertex_t l) {
  list_vertex_t p=calloc(1,sizeof(*p));
  if (NULL==p){
    fprintf(stderr,"Fatal: Unable to allocate new list link.\n");
    return l;
  }
  list_vertex_t l1=list_vertex_new();
  while(!list_vertex_is_empty(l)){
    l1=list_vertex_add_first(l->val,l1);
    l=list_vertex_del_first(l);
  }
  l1=list_vertex_add_first(e,l1);
  while(!list_vertex_is_empty(l1)){
    l=list_vertex_add_first(l1->val,l);
    l1=list_vertex_del_first(l1);
  }
  free(p);
  return(l);
}

list_vertex_t list_vertex_delete(list_vertex_t l) {
  while(!list_vertex_is_empty(l)){
    l=list_vertex_del_first(l);
  }
  return(l);
}


list_vertex_t list_vertex_concat(list_vertex_t l1, list_vertex_t l2) {
  if (list_vertex_is_empty(l1)){
    return(l2);
  }
  if (list_vertex_is_empty(l2)){
    return(l1);
  }
  list_vertex_t point=l2;
  while(!list_vertex_is_empty(point)){
    l1=list_vertex_add_last(point->val,l1);
    point=point->next;
  }
  return(l1);
}



int list_vertex_find_less_cost(list_vertex_t l, int* somnumero){
  list_vertex_t c=l;
  list_vertex_t sel=l;
  int i=0;
  int n=0;
  if(l->next==NULL){
    *somnumero = l->val->numero;
  }
  while(!list_vertex_is_empty(c)){
    if(c->val->cout<sel->val->cout){
      sel=c;
      n=i;
    }
    c=c->next;
    i++;
  }
  //mettre somnumero sur le numero du sommet
  *somnumero = sel->val->numero;
  return(n);
}

vertex_t* list_vertex_find_vertex_n(int n, list_vertex_t l){
  int i;
  list_vertex_t p=l;
  for(i=0;i<n;i++){
    p=p->next;
  }
  return(p->val);
}

list_vertex_t list_vertex_remove_n(int n, list_vertex_t l) {
  if (list_vertex_is_empty(l)){
    l=NULL;
    return(NULL);
  }

  int length=list_vertex_length(l);
  if (n>length){
    return(l);
  }

  if (n<0){
    return(l);
  }
  if (n==0){
    l=list_vertex_del_first(l);
    return(l);
  }
  int i;
  list_vertex_t p1=l;
  list_vertex_t p2=l->next;
  for(i=0;i<n-1;i++){
    p1=p1->next;
  }
  p2=p1->next;
  p1->next=p1->next->next;
  p2->next=NULL;
  p2=list_vertex_delete(p2);
  return(l);
}
