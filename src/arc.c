#include "arc.h"

edge_t arc_new (int arrivee, double cout){
  edge_t arc;
  arc.arrivee=arrivee;
  arc.cout=cout;
  return(arc);
}


void arc_print(edge_t arc){
  printf("{%d,%lf}",arc.arrivee, arc.cout);
}

int arc_compare(edge_t arc1, edge_t arc2){
  if((arc1.arrivee==arc2.arrivee) && (arc1.cout==arc2.cout)){
    return(1);
  }
  else{
    return(0);
  }
}
