#include "ProgrammeFinal.h"

int choix_fichier(){
  int choix2=0;

  while((choix2!=1) && (choix2!=2) && (choix2!=3) && (choix2!=4) && (choix2!=5) && (choix2!=6)&& (choix2!=7) && (choix2!=8) && (choix2!=9) && (choix2!=10)){

    printf("choix du graphe à tester:\n\n");
    printf("Tapez 1: graphe1.txt\n");
    printf("Tapez 2: graphe2.txt\n");
    printf("Tapez 3: grapheColorado.txt\n");
    printf("Tapez 4: grapheFloride.txt\n");
    printf("Tapez 5: grapheGrandLacs.txt\n");
    printf("Tapez 6: grapheMonde.txt\n");
    printf("Tapez 7: grapheNewYork.txt\n");
    printf("Tapez 8: grapheUSACentral.txt\n");
    printf("Tapez 9: grapheUSAOuest.txt\n");
    printf("Tapez 10: metroetu.txt\n");
    scanf("%d",&choix2);

    if((choix2!=1) && (choix2!=2) && (choix2!=3) && (choix2!=4) && (choix2!=5) && (choix2!=6)&& (choix2!=7) && (choix2!=8) && (choix2!=9) && (choix2!=10)){
      printf("Saisie non valide\n");
    }
    //return(fichier_graph);
  }

  switch(choix2){
    case 1:
      return(1);
      break;
    case 2:
      return(2);
      break;
    case 3:
      return(3);
      break;
    case 4:
      return(4);
      break;
    case 5:
      return(5);
      break;
    case 6:
      return(6);
      break;
    case 7:
      return(7);
      break;
    case 8:
      return(8);
      break;
    case 9:
      return(9);
      break;
    case 10:
      return(10);
      break;
    default:
      return(0);
      break;
  }
}

int ProgrammeFinal(){

  int n;
  int choix;
  int choix1=0;
  int choixfichier;
  int cl;
  graph_t graph;
  int depart;
  int arrivee;
  list_vertex_t listpere;

  while((choix1!=1) && (choix1!=2) && (choix1!=3) && (choix1!=4) && (choix1!=5) && (choix1!=6)){
    printf("Veuillez choisir l'algorythme que vous souhaitez tester:\n\n");
    printf("Tapez 1 : Dijkstra avec gestion par liste de sommets\n");
    printf("Tapez 2 : Dijkstra avec gestion par tas de sommets\n");
    printf("Tapez 3 : Astar avec gestion par liste de sommets\n");
    printf("Tapez 4 : Astar avec gestion par tas de sommets\n");
    printf("Tapez 5 : Recherche du plus faible cout et chemin par nom\n");
    printf("Tapez 6 : Moyenne temporelle\n\n");
    scanf("%d",&choix1);
    if((choix1!=1) && (choix1!=2) && (choix1!=3) && (choix1!=4) && (choix1!=5) && (choix1!=6)){
      printf("Saisie non valide\n");
    }
  }

  switch(choix1){
    case 1 :

      choixfichier=choix_fichier();

      printf("\n\n");
      printf("Choisissez le numéro de sommet de depart (attention à ce qu'il soit bien dans le graph):");
      scanf("%d",&depart);
      printf("Choisissez le numéro de sommet d'arrivee (attention à ce qu'il soit bien dans le graph):");
      scanf("%d",&arrivee);

      switch(choixfichier){
          case 1:
            graph = readgraph("DATA_projet2021/graphe1.txt",&graph);
            break;

          case 2:
            graph = readgraph("DATA_projet2021/graphe2.txt",&graph);
            break;

          case 3:
            graph = readgraph("DATA_projet2021/grapheColorado.txt",&graph);
            break;

          case 4:
            graph = readgraph("DATA_projet2021/grapheFloride.txt",&graph);
            break;

          case 5:
            graph = readgraph("DATA_projet2021/grapheGrandLacs.txt",&graph);
            break;

          case 6:
            graph = readgraph("DATA_projet2021/grapheMonde.txt",&graph);
            break;

          case 7:
            graph = readgraph("DATA_projet2021/grapheNexYork.txt",&graph);
            break;

          case 8:
            graph = readgraph("DATA_projet2021/grapheUSACentral.txt",&graph);
            break;

          case 9:
            graph = readgraph("DATA_projet2021/grapheUSAOuest.txt",&graph);
            break;

          case 10:
            graph = readgraph("DATA_projet2021/metroetu.txt",&graph);
            break;
      }

      graph_print(graph);
      getchar();

      printf("===========================================\n");
      printf("La fonction Dijkstra_list fonctionne correctement: ");
      cl=clock();

      if(Dijkstra(depart,arrivee,graph)==1){
        printf(" OUI\n\n");
      }
      else{printf(" NON\n\n");}
      cl = clock()-cl;
      printf("===========================================\n");
      printf("Temps mesure en secondes: %lf\n",cl/(double)CLOCKS_PER_SEC);
      printf("===========================================\n");
      getchar();

      printf("===========================================\n");
      printf("Valeur du plus court chemin:");
      printf("%lf\n", graph.data[arrivee].cout);

      listpere=list_vertex_new();
      listpere=list_vertex_add_first(&graph.data[arrivee],listpere);
      listpere=chemin_inverse(graph, depart, arrivee, &listpere);

      getchar();

      printf("===========================================\n");
      printf("AFFICHAGE DU CHEMIN\n");
      list_vertex_print(listpere);

      getchar();

      printf("===========================================\n");
      printf("FIN DU TEST\n\n");
      printf("===========================================\n\n\n");

      listpere=list_vertex_delete(listpere);
      graph_del(graph);

      return(1);
      break;

    case 2 :

      choixfichier=choix_fichier();

      printf("\n\n");
      printf("Choisissez le numéro de sommet de depart (attention à ce qu'il soit bien dans le graph):");
      scanf("%d",&depart);
      printf("Choisissez le numéro de sommet d'arrivee (attention à ce qu'il soit bien dans le graph):");
      scanf("%d",&arrivee);

      switch(choixfichier){
          case 1:
            graph = readgraph("DATA_projet2021/graphe1.txt",&graph);
            break;

          case 2:
            graph = readgraph("DATA_projet2021/graphe2.txt",&graph);
            break;

          case 3:
            graph = readgraph("DATA_projet2021/grapheColorado.txt",&graph);
            break;

          case 4:
            graph = readgraph("DATA_projet2021/grapheFloride.txt",&graph);
            break;

          case 5:
            graph = readgraph("DATA_projet2021/grapheGrandLacs.txt",&graph);
            break;

          case 6:
            graph = readgraph("DATA_projet2021/grapheMonde.txt",&graph);
            break;

          case 7:
            graph = readgraph("DATA_projet2021/grapheNexYork.txt",&graph);
            break;

          case 8:
            graph = readgraph("DATA_projet2021/grapheUSACentral.txt",&graph);
            break;

          case 9:
            graph = readgraph("DATA_projet2021/grapheUSAOuest.txt",&graph);
            break;

          case 10:
            graph = readgraph("DATA_projet2021/metroetu.txt",&graph);
            break;
      }

      graph_print(graph);
      getchar();

      printf("===========================================\n");
      printf("La fonction Dijkstra_tas fonctionne correctement: ");
      cl=clock();

      if(Dijkstra_tas(depart,arrivee,graph)==1){
        printf(" OUI\n\n");
      }
      else{printf(" NON\n\n");}
      cl = clock()-cl;
      printf("===========================================\n");
      printf("Temps mesure en secondes: %lf\n",cl/(double)CLOCKS_PER_SEC);
      printf("===========================================\n");
      getchar();

      printf("===========================================\n");
      printf("Valeur du plus court chemin:");
      printf("%lf\n", graph.data[arrivee].cout);

      listpere=list_vertex_new();
      listpere=list_vertex_add_first(&graph.data[arrivee],listpere);
      listpere=chemin_inverse(graph, depart, arrivee, &listpere);

      getchar();

      printf("===========================================\n");
      printf("AFFICHAGE DU CHEMIN\n");
      list_vertex_print(listpere);

      getchar();

      printf("===========================================\n");
      printf("FIN DU TEST\n\n");
      printf("===========================================\n\n\n");

      listpere=list_vertex_delete(listpere);
      graph_del(graph);

      return(1);
      break;

    case 3 :

      choixfichier=choix_fichier();

      printf("\n\n");
      printf("Choisissez le numéro de sommet de depart (attention à ce qu'il soit bien dans le graph):");
      scanf("%d",&depart);
      printf("Choisissez le numéro de sommet d'arrivee (attention à ce qu'il soit bien dans le graph):");
      scanf("%d",&arrivee);

      switch(choixfichier){
          case 1:
            graph = readgraph("DATA_projet2021/graphe1.txt",&graph);
            break;

          case 2:
            graph = readgraph("DATA_projet2021/graphe2.txt",&graph);
            break;

          case 3:
            graph = readgraph("DATA_projet2021/grapheColorado.txt",&graph);
            break;

          case 4:
            graph = readgraph("DATA_projet2021/grapheFloride.txt",&graph);
            break;

          case 5:
            graph = readgraph("DATA_projet2021/grapheGrandLacs.txt",&graph);
            break;

          case 6:
            graph = readgraph("DATA_projet2021/grapheMonde.txt",&graph);
            break;

          case 7:
            graph = readgraph("DATA_projet2021/grapheNexYork.txt",&graph);
            break;

          case 8:
            graph = readgraph("DATA_projet2021/grapheUSACentral.txt",&graph);
            break;

          case 9:
            graph = readgraph("DATA_projet2021/grapheUSAOuest.txt",&graph);
            break;

          case 10:
            graph = readgraph("DATA_projet2021/metroetu.txt",&graph);
            break;
      }

      graph_print(graph);
      getchar();

      printf("===========================================\n");
      printf("La fonction Astar_list fonctionne correctement: ");
      cl=clock();

      if(Astar(depart,arrivee,graph)==1){
        printf(" OUI\n\n");
      }
      else{printf(" NON\n\n");}
      cl = clock()-cl;
      printf("===========================================\n");
      printf("Temps mesure en secondes: %lf\n",cl/(double)CLOCKS_PER_SEC);
      printf("===========================================\n");
      getchar();

      printf("===========================================\n");
      printf("Valeur du plus court chemin:");
      printf("%lf\n", graph.data[arrivee].cout);

      listpere=list_vertex_new();
      listpere=list_vertex_add_first(&graph.data[arrivee],listpere);
      listpere=chemin_inverse(graph, depart, arrivee, &listpere);

      getchar();

      printf("===========================================\n");
      printf("AFFICHAGE DU CHEMIN\n");
      list_vertex_print(listpere);

      getchar();

      printf("===========================================\n");
      printf("FIN DU TEST\n\n");
      printf("===========================================\n\n\n");

      listpere=list_vertex_delete(listpere);
      graph_del(graph);

      return(1);
      break;

    case 4 :

      choixfichier=choix_fichier();

      printf("\n\n");
      printf("Choisissez le numéro de sommet de depart (attention à ce qu'il soit bien dans le graph):");
      scanf("%d",&depart);
      printf("Choisissez le numéro de sommet d'arrivee (attention à ce qu'il soit bien dans le graph):");
      scanf("%d",&arrivee);

      switch(choixfichier){
          case 1:
            graph = readgraph("DATA_projet2021/graphe1.txt",&graph);
            break;

          case 2:
            graph = readgraph("DATA_projet2021/graphe2.txt",&graph);
            break;

          case 3:
            graph = readgraph("DATA_projet2021/grapheColorado.txt",&graph);
            break;

          case 4:
            graph = readgraph("DATA_projet2021/grapheFloride.txt",&graph);
            break;

          case 5:
            graph = readgraph("DATA_projet2021/grapheGrandLacs.txt",&graph);
            break;

          case 6:
            graph = readgraph("DATA_projet2021/grapheMonde.txt",&graph);
            break;

          case 7:
            graph = readgraph("DATA_projet2021/grapheNexYork.txt",&graph);
            break;

          case 8:
            graph = readgraph("DATA_projet2021/grapheUSACentral.txt",&graph);
            break;

          case 9:
            graph = readgraph("DATA_projet2021/grapheUSAOuest.txt",&graph);
            break;

          case 10:
            graph = readgraph("DATA_projet2021/metroetu.txt",&graph);
            break;
      }

      graph_print(graph);
      getchar();

      printf("===========================================\n");
      printf("La fonction Astar_tas fonctionne correctement: ");
      cl=clock();

      if(Astar_tas(depart,arrivee,graph)==1){
        printf(" OUI\n\n");
      }
      else{printf(" NON\n\n");}
      cl = clock()-cl;
      printf("===========================================\n");
      printf("Temps mesure en secondes: %lf\n",cl/(double)CLOCKS_PER_SEC);
      printf("===========================================\n");
      getchar();

      printf("===========================================\n");
      printf("Valeur du plus court chemin:");
      printf("%lf\n", graph.data[arrivee].cout);

      listpere=list_vertex_new();
      listpere=list_vertex_add_first(&graph.data[arrivee],listpere);
      listpere=chemin_inverse(graph, depart, arrivee, &listpere);

      getchar();

      printf("===========================================\n");
      printf("AFFICHAGE DU CHEMIN\n");
      list_vertex_print(listpere);

      getchar();

      printf("===========================================\n");
      printf("FIN DU TEST\n\n");
      printf("===========================================\n\n\n");

      listpere=list_vertex_delete(listpere);
      graph_del(graph);

      return(1);
      break;

    case 5 :

      choixfichier=choix_fichier();

      printf("\n\n");


      switch(choixfichier){
          case 1:
            graph = readgraph("DATA_projet2021/graphe1.txt",&graph);
            break;

          case 2:
            graph = readgraph("DATA_projet2021/graphe2.txt",&graph);
            break;

          case 3:
            graph = readgraph("DATA_projet2021/grapheColorado.txt",&graph);
            break;

          case 4:
            graph = readgraph("DATA_projet2021/grapheFloride.txt",&graph);
            break;

          case 5:
            graph = readgraph("DATA_projet2021/grapheGrandLacs.txt",&graph);
            break;

          case 6:
            graph = readgraph("DATA_projet2021/grapheMonde.txt",&graph);
            break;

          case 7:
            graph = readgraph("DATA_projet2021/grapheNexYork.txt",&graph);
            break;

          case 8:
            graph = readgraph("DATA_projet2021/grapheUSACentral.txt",&graph);
            break;

          case 9:
            graph = readgraph("DATA_projet2021/grapheUSAOuest.txt",&graph);
            break;

          case 10:
            graph = readgraph("DATA_projet2021/metroetu.txt",&graph);
            break;
      }

      graph_print(graph);

      int verif;
      int numdepart;
      int numarrivee;
      verif=metro(graph,&numdepart,&numarrivee);

      printf("===========================================\n");
      printf("La fonction metro fonctionne correctement: ");
      cl=clock();

      if(verif==1){

        printf(" OUI\n\n");
      }
      else{
        printf(" NON\n\n");
        graph_del(graph);
        return(0);
      }

      getchar();

      list_vertex_t listpere=list_vertex_new();


      listpere=list_vertex_add_first(&graph.data[numarrivee],listpere);
      listpere=chemin_inverse(graph, numdepart, numarrivee, &listpere);
      listpere=chemin_inverse_del_double_arrivee(listpere);

      printf("===========================================\n");
      printf("Valeur du plus court chemin:");
      list_vertex_t pointeur=list_vertex_new();
      pointeur=listpere;
      while(!list_vertex_is_empty(pointeur->next)){
        pointeur=pointeur->next;
      }
      double cout;
      cout=(pointeur->val->cout)
      -(listpere->val->cout);
      printf("%lf\n\n", cout);
      getchar();


      printf("===========================================\n");
      printf("AFFICHAGE DU CHEMIN\n");

      list_vertex_line_print(listpere);
      cl = clock()-cl;
      printf("===========================================\n");
      printf("Temps mesure en secondes: %lf\n",cl/(double)CLOCKS_PER_SEC);

      getchar();

      printf("===========================================\n");
      printf("FIN DU TEST\n\n");
      printf("===========================================\n\n\n");

      listpere=list_vertex_delete(listpere);
      graph_del(graph);

      return(1);
      break;

    case 6 :

      choix=0;

      while((choix!=1) && (choix!=2) && (choix!=3) && (choix!=4)){
        printf("Choix de l'algorithme:\n");
        printf("Tapez 1 : Dijkstra avec gestion par liste de sommets\n");
        printf("Tapez 2 : Dijkstra avec gestion par tas de sommets\n");
        printf("Tapez 3 : Astar avec gestion par liste de sommets\n");
        printf("Tapez 4 : Astar avec gestion par tas de sommets\n\n");
        scanf("%d",&choix);
        if((choix!=1) && (choix!=2) && (choix!=3) && (choix!=4)){printf("Saisie non valide\n");}
      }

      printf("Choisir le nombre de donner que vous voulez utiliser pour votre statistique:");
      scanf("%d",&n);
      int i;
      double moy=0;

      cl=clock();
      choixfichier=choix_fichier();

      printf("\n\n");

      switch(choixfichier){
          case 1:
            graph = readgraph("DATA_projet2021/graphe1.txt",&graph);
            break;

          case 2:
            graph = readgraph("DATA_projet2021/graphe2.txt",&graph);
            break;

          case 3:
            graph = readgraph("DATA_projet2021/grapheColorado.txt",&graph);
            break;

          case 4:
            graph = readgraph("DATA_projet2021/grapheFloride.txt",&graph);
            break;

          case 5:
            graph = readgraph("DATA_projet2021/grapheGrandLacs.txt",&graph);
            break;

          case 6:
            graph = readgraph("DATA_projet2021/grapheMonde.txt",&graph);
            break;

          case 7:
            graph = readgraph("DATA_projet2021/grapheNewYork.txt",&graph);
            break;

          case 8:
            graph = readgraph("DATA_projet2021/grapheUSACentral.txt",&graph);
            break;

          case 9:
            graph = readgraph("DATA_projet2021/grapheUSAOuest.txt",&graph);
            break;

          case 10:
            graph = readgraph("DATA_projet2021/metroetu.txt",&graph);
            break;
      }

      cl = clock()-cl;
      moy += cl/(double)CLOCKS_PER_SEC;
      printf("===================================\n\n");
      printf("le temps de création du graphe est : %lf\n\n", moy);
      printf("===================================\n\n");

      moy=0;

      for(i=0;i<n;i++){
        int depart = random()%graph.size_vertices;
        int arrivee = random()%graph.size_vertices;
        cl=clock();
        switch(choix){
          case 1:
            Dijkstra(depart,arrivee,graph);
            break;
          case 2:
            Dijkstra_tas(depart,arrivee,graph);
            break;
          case 3:
            Astar(depart,arrivee,graph);
            break;
          case 4:
            Astar_tas(depart,arrivee,graph);
            break;
        }
        cl = clock()-cl;
        moy += cl/(double)CLOCKS_PER_SEC;

      }

        moy=moy/n;

        printf("===================================\n\n");
        printf("le temps de calcul moyen pour ce test sur ce test est : %lf\n\n", moy);
        printf("===================================\n\n");
        graph_del(graph);
        return(1);
        break;


    default :
      return(0);
      break;
  }
  return EXIT_SUCCESS;
}
