#include "Dijkstra_list.h"
#include "list_vertex.h"
#include <float.h>

int Dijkstra(int depart, int arrivee, graph_t graph){
//A RAJOUTER SI ON VEUT VERIFIER L'APPARTENANCE DES SOMMETS ARRIVEE ET DEPART AU graph
  // if (graph_find_vertex(graph, depart) == 0){
  //   fprintf( stderr, "DEPART IS NOT IN GRAPH\n" );
  //   return 0;
  // }
  // if (graph_find_vertex(graph, arrivee) == 0){
  //   fprintf( stderr, "ARRIVEE IS NOT IN GRAPH\n" );
  //   return 0;
  // }
  //

  list_vertex_t Atraiter = list_vertex_new();
  list_vertex_t Atteint = list_vertex_new();
  vertex_t* u;
  int unumero;
  int somnumero =0;
  double disttemp;
  int i;


  // INITIALISATION DES COUTS A L'INFINI DEFINI PAR -1
  for (i=0; i<graph.size_vertices; i++){
      graph.data[i].pcc = DBL_MAX;
      graph.data[i].cout = DBL_MAX;
  }


  graph.data[depart].pcc = 0;
  graph.data[depart].cout = 0;



  // ON AJOUTE LE SOMMET DE DEPART DANS ATRAITER
  Atraiter = list_vertex_add_first(&graph.data[depart],Atraiter);

  //TANT QUE ATRAITER N'EST PAS VIDE
  while (!list_vertex_is_empty(Atraiter)){

    //recherche du sommet de plus faible cout dans ATRAITER et suppression de ce sommet

    //somnumero va contenir l'indice du sommet dans le graphe et unumero son indice dans Atraiter
    unumero = list_vertex_find_less_cost(Atraiter, &somnumero);

    //ici on utilise l'indice du sommet propre au graphe
    u = &graph.data[somnumero];
    //ici on utilise l'indice d'emplacement du sommet dans ATRAITER
    Atraiter = list_vertex_remove_n(unumero,Atraiter);

    if (somnumero == arrivee){
      Atteint=list_vertex_delete(Atteint);
      Atraiter=list_vertex_delete(Atraiter);
      //solution trouvée
      return 1;
    }
    //Sinon on a pas trouvé la solution mais le plus court chemin de départ à u
    else{

      //on ajoute ce sommet dans la liste des sommets atteints
      Atteint = list_vertex_add_first(u,Atteint);

      //boucle sur tous les sommets voisins de u

      listedge_t pointeur=u->edges;

        while(!(listedge_is_empty(pointeur))){

          disttemp=u->pcc + pointeur->val.cout;
          vertex_t* v = &graph.data[pointeur->val.arrivee];

          if ((list_vertex_find(v->numero, Atteint) == NULL) && (disttemp < v->pcc)){

            v->cout = disttemp;
            v->pcc = disttemp;
            //Ajout du père dans le liste_vertex graph_find_vertex
            v->pere=u->numero;
            //et on ajoute le sommet v dans atraiter
            Atraiter = list_vertex_add_first(v, Atraiter);
          }
          pointeur=pointeur->next;
        }
    }
  }

  Atraiter=list_vertex_delete(Atraiter);
  Atteint=list_vertex_delete(Atteint);


  if (graph.data[graph_find_vertex(graph,arrivee)].cout != DBL_MAX){
    return 1;
  }
  return 0;
}
