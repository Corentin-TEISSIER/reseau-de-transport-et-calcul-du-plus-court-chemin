#include "Astar_tas.h"
#include "tas_vertex.h"
#include <float.h>

int Astar_tas(int depart, int arrivee, graph_t graph){
//A RAJOUTER SI ON VEUT VERIFIER L'APPARTENANCE DES SOMMETS ARRIVEE ET DEPART AU graph
  // if (graph_find_vertex(graph, depart) == 0){
  //   fprintf( stderr, "DEPART IS NOT IN GRAPH\n" );
  //   return 0;
  // }
  // if (graph_find_vertex(graph, arrivee) == 0){
  //   fprintf( stderr, "ARRIVEE IS NOT IN GRAPH\n" );
  //   return 0;
  // }
  //

  tas_vertex_t Atraiter = tas_vertex_new(graph.size_edges);
  tas_vertex_t Atteint = tas_vertex_new(graph.size_vertices);
  vertex_t* u;
  double disttemp;
  int i;


  // INITIALISATION DES COUTS A L'INFINI DEFINI PAR -1
  for (i=0; i<graph.size_vertices; i++){
      graph.data[i].pcc = DBL_MAX;
      graph.data[i].cout = DBL_MAX;
  }


  graph.data[depart].pcc = 0;
  graph.data[depart].cout = 0;



  // ON AJOUTE LE SOMMET DE DEPART DANS ATRAITER
  Atraiter = tas_vertex_add(&graph.data[depart],&Atraiter);

  //TANT QUE ATRAITER N'EST PAS VIDE
  while (!tas_vertex_is_empty(Atraiter)){


    //recherche du sommet de plus faible cout dans ATRAITER et suppression de ce sommet

    u=tas_vertex_get_min(Atraiter);
    tas_vertex_delete_min(&Atraiter);

    if (u->numero == arrivee){
      tas_vertex_delete(Atteint);
      tas_vertex_delete(Atraiter);
      //solution trouvée
      return 1;
    }
    //Sinon on a pas trouvé la solution mais le plus court chemin de départ à u
    else{

      //on ajoute ce sommet dans la liste des sommets atteints
      Atteint = tas_vertex_add(u,&Atteint);

      //boucle sur tous les sommets voisins de u

      listedge_t pointeur=u->edges;

        while(!(listedge_is_empty(pointeur))){

          disttemp=u->pcc + pointeur->val.cout;
          vertex_t* v = &graph.data[pointeur->val.arrivee];

          if ((tas_vertex_find(v,Atteint)==0) && (disttemp < v->pcc)){

            v->cout = disttemp+vertex_heuristique(*v,graph.data[arrivee]);
            v->pcc = disttemp;
            //Ajout du père dans le liste_vertex graph_find_vertex
            v->pere=u->numero;
            //et on ajoute le sommet v dans atraiter
            Atraiter = tas_vertex_add(v, &Atraiter);


          }
          pointeur=pointeur->next;
        }
    }
  }

  tas_vertex_delete(Atraiter);
  tas_vertex_delete(Atteint);


  if (graph.data[graph_find_vertex(graph,arrivee)].cout != DBL_MAX){
    return 1;
  }
  return 0;
}
