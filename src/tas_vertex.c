#include "tas_vertex.h"

tas_vertex_t tas_vertex_new(int m) {
  tas_vertex_t tas;
  tas.max_size=m;
  tas.number=0;
  tas.data=malloc(m*sizeof(vertex_t*));
  int i;
  for(i=0;i<m;i++){
    tas.data[i]=NULL;
  }
  return tas;
}


int tas_vertex_is_empty(tas_vertex_t tas) {
  return(tas.number==0);
}

/* Ajoute l'element valeur au tas pointe par ptas */

//okok
tas_vertex_t tas_vertex_add(vertex_t* sommet, tas_vertex_t* ptas) {
  if (ptas->number == ptas->max_size) {return *ptas;}
  int i;
  i=ptas->number;
  ptas->number++;
  ptas->data[i]=sommet;
  while(i>=0){
    int j=(i-1)/2;
    if(ptas->data[j]->cout>ptas->data[i]->cout){
      vertex_t* tampon;
      tampon=ptas->data[j];
      ptas->data[j]=ptas->data[i];
      ptas->data[i]=tampon;
      i=j;
    }
    else{
      return(*ptas);
    }
  }
  return(*ptas);
}


/* Retourne l'indice du plus grand des deux fils ou -1 si c'est une feuille */
//Pb de segmentation fault au test venant d'ici d'après GDB
int tas_vertex_smallest_son(tas_vertex_t tas, int indice) {
  if(((2*indice+1)>tas.number) && ((2*(indice+1))>tas.number)){
    return(-1);
  }
  else if(2*(indice+1)>tas.number){
    return(2*indice+1);
  }
  else{
    if(tas.data[2*indice+1]<tas.data[2*(indice+1)]){
      return(2*indice+1);
    }
    else{
      return(2*(indice+1));
    }
  }
}



/* Supprimer la racine en la remplacant par le dernier element du tas et en réorganisation le tas */
int tas_vertex_delete_min(tas_vertex_t* ptas) {
  int i=0;
  int j;
  if(ptas->number==0){
    return(0);
  }
  if(tas_vertex_smallest_son(*ptas,0)==-1){
    ptas->number=0;
    ptas->data=NULL;
    return(1);
  }
  ptas->data[0]=ptas->data[ptas->number-1];
  ptas->number--;
  while(!(tas_vertex_smallest_son(*ptas,i)==-1)){
    j=tas_vertex_smallest_son(*ptas,i);
    if(ptas->data[j]->cout<ptas->data[i]->cout){
      vertex_t* tampon;
      tampon=ptas->data[j];
      ptas->data[j]=ptas->data[i];
      ptas->data[i]=tampon;
      i=j;
    }
    else{
      return(1);
    }
  }
  return 0;
}

/* Libere la memoire allouee par tas_vertex_new */
void tas_vertex_delete(tas_vertex_t tas) {
  free(tas.data);
}

/* Retourne l'element max sans le suprimer du tas */
vertex_t* tas_vertex_get_min(tas_vertex_t tas) {
  return tas.data[0];
}

/* Affiche le tas a l'ecran */
void tas_vertex_print(tas_vertex_t tas) {
  int i;
  printf("----------DEBUT DU TAS----------\n\n");
  for(i=0; i<tas.number; i++){
    vertex_print(*(tas.data[i]));
    puts("");
  }
  printf("----------FIN DU TAS----------\n\n");
}


int tas_vertex_find(vertex_t* v,tas_vertex_t tas){
  int i;
  for(i=0;i<tas.number;i++){
    if(tas.data[i]->numero==v->numero){
      return(1);
      i++;
    }
  }
  return(0);
}





/*
  Verifie la propriete des tas sur tous les noeuds :
  le pere est plus grand que les 2 fils
*/

// int tas_vertex_verification(tas_vertex_t tas) {
//   int i;
//   for (i=0; i< tas.number/2; i++) {
//   if (ELEMENT_COMPARE(tas.data+i,tas.data+tas_vertex_LEFTSON(i))<0) return i;
//     if (tas_vertex_RIGHTSON(i)<tas.number && tas.data[i]<tas.data[tas_vertex_RIGHTSON(i)]) return i;
//   }
//   return -1;
// }
