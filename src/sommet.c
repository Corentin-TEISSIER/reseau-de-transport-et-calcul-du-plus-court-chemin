#include "sommet.h"

void vertex_print(vertex_t v){
  printf("\n\n//\n");
  printf("affichage du sommet numéro: %d\nnom: %s\nligne: %s\ncoordonnées: (%lf,%lf)\n\n",v.numero,v.nom,v.ligne,v.x,v.y);
  printf("ensemble des destinations possibles depuis ce sommet:\n");
  listedge_print(v.edges);
  printf("\n");
  //decomenter la ligne suivante pour afficher les couts
  //printf("COUT: %lf",v.cout);
  printf("//\n\n");
}

double vertex_heuristique(vertex_t v1,vertex_t v2){
  return(sqrt((v2.x-v1.x)*(v2.x-v1.x)+(v2.y-v1.y)*(v2.y-v1.y)));
}

vertex_t vertex_add_edge(vertex_t sommet, edge_t arc){
  sommet.edges=listedge_add_last(arc,sommet.edges);
  return(sommet);
}

int vertex_access(vertex_t v1, vertex_t v2){
  listedge_t p=listedge_find(v2.numero,v1.edges);
  if(p==NULL){
    return 0;
  }
  else{
    return 1;
  }
}

double vertex_cost(vertex_t v1,vertex_t v2){
  assert(vertex_access(v1,v2));
  listedge_t p=listedge_new();
  p=listedge_find(v2.numero,v1.edges);
  return (p->val.cout);
}

int vertex_equal(vertex_t* v1, vertex_t* v2){
  if(v1->numero==v2->numero && v1->nom==v2->nom && v1->ligne==v2->ligne && v1->x==v2->x && v1->y==v2->y){
    return(1);
  }
  return(0);
}
