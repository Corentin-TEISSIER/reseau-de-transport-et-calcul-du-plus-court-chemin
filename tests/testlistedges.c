#include "listedges.h"




int main(){

  listedge_t list1;
  listedge_t list2;
  listedge_t p1;
  int longueur;

  edge_t e1;
  edge_t e2;
  edge_t e3;


  e1 = arc_new(1,5);
  e2 = arc_new(2,3);
  e3 = arc_new(3,10);



  list1 = listedge_add_first(e1, list1);
  list1 = listedge_add_first(e2, list1);
  list1 = listedge_add_first(e3, list1);

  listedge_print(list1);

  list1 = listedge_del_first(list1);

  listedge_print(list1);

  p1 = listedge_find(2, list1);

  if(!listedge_is_empty(p1)){
    printf("Un arc menant au sommet indice :%d a été trouvé dans la liste 1 et le coût vaut :%lf\n", p1->val.arrivee, p1->val.cout);
  }

  longueur = listedge_length(list1);

  printf("La longueur de la liste 1 est de :%d\n",longueur);


  printf("On ajoute l'arc e1 à la fin de la liste 1\n");

  list1 = listedge_add_last(e1, list1);

  listedge_print(list1);

  printf("On copie la liste 1 dans la liste 2\n");
  list2 = listedge_copy(list1);

  printf("On affiche la liste 2 pour vérifier la copie\n");
  listedge_print(list2);

  printf("Suppression de la liste 1\n");
  list1 = listedge_delete(list1);

  printf("On vérifie sa suppression en l'affichant :\n");
  listedge_print(list1);

  printf("On affiche la liste 2\n");
  listedge_print(list2);
}
