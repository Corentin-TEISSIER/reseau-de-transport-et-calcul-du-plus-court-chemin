#include "Dijkstra_tas.h"
#include "chemin_inverse.h"
#include <time.h>
int main(int argc, char *argv[]){

  if ( argc < 4 ) {
    fprintf( stderr, "Usage %s fichier_graphe numéro_départ numéro_arrivée\n", argv[0] );
    exit( EXIT_FAILURE );
    }

  graph_t graph = readgraph(argv[1],&graph);

  graph_print(graph);
  getchar();

  int depart = atoi(argv[2]);
  int arrivee = atoi(argv[3]);

  printf("===========================================\n");
  printf("La fonction Dijkstra fonctionne correctement: ");
  unsigned long cl;
  cl=clock();
  if(Dijkstra_tas(depart,arrivee,graph)==1){
    printf(" OUI\n\n");
  }
  else{printf(" NON\n\n");}
  cl = clock()-cl;
  
  getchar();

  printf("===========================================\n");
  printf("Valeur du plus court chemin:");
  printf("%lf\n", graph.data[arrivee].cout);

  list_vertex_t listpere=list_vertex_new();
  listpere=list_vertex_add_first(&graph.data[arrivee],listpere);
  listpere=chemin_inverse(graph, depart, arrivee, &listpere);

  getchar();

  printf("===========================================\n");
  printf("AFFICHAGE DU CHEMIN\n");
  list_vertex_line_print(listpere);

  printf("===========================================\n");
  printf("Temps mesure en secondes: %lf\n",cl/(double)CLOCKS_PER_SEC);

  getchar();

  printf("===========================================\n");
  printf("FIN DU TEST\n\n");
  printf("===========================================\n\n\n");

  listpere=list_vertex_delete(listpere);
  graph_del(graph);

  return(EXIT_SUCCESS);
}
