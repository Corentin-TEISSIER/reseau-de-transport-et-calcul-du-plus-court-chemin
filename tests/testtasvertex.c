#include "tas_vertex.h"
#include "readgraph.h"
#include "graph.h"


int main(int argc, char *argv[]){

  graph_t graph;

  if ( argc < 2 ) {

    fprintf( stderr, "Usage %s ficher_graphe\n", argv[0] );
    exit( EXIT_FAILURE );
  }

  //lecture du fichier et remplissage du graph
  graph=readgraph(argv[1],&graph);

  int i=0;
  vertex_t* x=&graph.data[i]; x->cout= 9; i++;
  tas_vertex_t tas;
  tas=tas_vertex_new(graph.size_vertices);

  x=&graph.data[i]; x->cout= 8; i++; printf("Ajout de "); vertex_print(*x);
  tas_vertex_add(x,&tas);
  printf("Nouveau tas : ");  tas_vertex_print(tas);
  puts("---------------");

  x=&graph.data[i]; x->cout= 7; i++; printf("Ajout de "); vertex_print(*x);
  tas_vertex_add(x,&tas);
  printf("Nouveau tas : ");  tas_vertex_print(tas);
  puts("---------------");

  x=&graph.data[i]; x->cout= 6; i++; printf("Ajout de "); vertex_print(*x);
  tas_vertex_add(x,&tas);
  printf("Nouveau tas : ");  tas_vertex_print(tas);
  puts("---------------");

  x=&graph.data[i]; x->cout= 5; i++; printf("Ajout de "); vertex_print(*x);
  tas_vertex_add(x,&tas);
  printf("Nouveau tas : ");  tas_vertex_print(tas);
  puts("---------------");

  x=&graph.data[i]; x->cout= 4; i++; printf("Ajout de "); vertex_print(*x);
  tas_vertex_add(x,&tas);
  printf("Nouveau tas : ");  tas_vertex_print(tas);
  puts("---------------");

  x=&graph.data[i]; x->cout= 3; i++; printf("Ajout de "); vertex_print(*x);
  tas_vertex_add(x,&tas);
  printf("Nouveau tas : ");  tas_vertex_print(tas);
  puts("---------------");

  x=&graph.data[i]; x->cout= 2; i++; printf("Ajout de "); vertex_print(*x);
  tas_vertex_add(x,&tas);
  printf("Nouveau tas : ");  tas_vertex_print(tas);
  puts("---------------");

  printf("Suppression de la racine :");
  x=tas_vertex_get_min(tas);
  vertex_print(*x);
  tas_vertex_delete_min(&tas);
  printf("Nouveau tas : ");  tas_vertex_print(tas);
  puts("---------------");

  printf("Suppression de la racine :");
  x=tas_vertex_get_min(tas);
  vertex_print(*x);
  tas_vertex_delete_min(&tas);
  printf("Nouveau tas : ");  tas_vertex_print(tas);
  puts("---------------");

  printf("Suppression de la racine :");
  x=tas_vertex_get_min(tas);
  vertex_print(*x);
  tas_vertex_delete_min(&tas);
  printf("Nouveau tas : ");  tas_vertex_print(tas);
  puts("---------------");

  printf("Est ce que le sommet 4 appartient encore au tas:");
  if (tas_vertex_find(&graph.data[4],tas)){
    printf("OUI\n\n");
  }
  else{
    printf("NON\n\n");
  }

  printf("Est ce que le sommet 6 appartient encore au tas:");
  if (tas_vertex_find(&graph.data[6],tas)){
    printf("OUI\n\n");
  }
  else{
    printf("NON\n\n");
  }

  printf("Affichage du plus petit sommet fils du sommmet 0:");
  vertex_print(*(tas.data[tas_vertex_smallest_son(tas,0)]));

  tas_vertex_delete_min(&tas);
  tas_vertex_delete_min(&tas);
  tas_vertex_delete_min(&tas);
  tas_vertex_delete_min(&tas);

  printf("Affichage d'un tas composé de zéro éléments:\n\n");

  tas_vertex_print(tas);

  printf("Vérifions que le programme sait que le tas est vide:");
  if(tas_vertex_is_empty(tas)){
    printf("  OUI\n\n");
  }
  else{
    printf("  NON\n\n");
  }

  tas_vertex_delete(tas);

  graph_del(graph);

  return EXIT_SUCCESS;
}
