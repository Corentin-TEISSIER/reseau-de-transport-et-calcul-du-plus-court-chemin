#include "graph.h"

int main(){

  //initialisation d'un graph
  graph_t graph=graph_init(2,7);

  //création de sommets à ajouter au graph
  vertex_t s0;
  s0.numero=0;
  s0.nom="Fred";
  s0.ligne="A";
  s0.x=0;
  s0.y=0;
  s0.edges=NULL;
  s0.pcc=0;
  edge_t e1=arc_new(1,5);
  s0=vertex_add_edge(s0,e1);
  edge_t e2=arc_new(2,10);
  s0=vertex_add_edge(s0,e2);
  edge_t e3=arc_new(3,15);
  s0=vertex_add_edge(s0,e3);
  edge_t e4=arc_new(4,20);
  s0=vertex_add_edge(s0,e4);

  vertex_t s1;
  s1.numero=1;
  s1.nom="George";
  s1.ligne="B";
  s1.x=10;
  s1.y=0;
  s1.edges=NULL;
  s1.pcc=0;
  edge_t e5=arc_new(0,5);
  s1=vertex_add_edge(s1,e5);
  edge_t e6=arc_new(2,3);
  s1=vertex_add_edge(s1,e6);
  edge_t e7=arc_new(4,15);
  s1=vertex_add_edge(s1,e7);


  //ajout des sommets au graph
  graph=graph_add_vertex(graph,s0);
  graph=graph_add_vertex(graph,s1);

  //affichage du graph pour vérification
  graph_print(graph);

  //recherche de l'indice du sommet numero "numéro" dans le graph "graph"
  printf("Le sommet s0 de numéro %d se trouve à l'indice %d du tableau de sommet du graph\n",s0.numero,graph_find_vertex(graph,s0.numero));
  printf("Le sommet s1 de numéro %d se trouve à l'indice %d du tableau de sommet du graph\n",s1.numero,graph_find_vertex(graph,s1.numero));

  printf("vérification du cas ou le sommet n'appartient pas au graph: validation du test:");
  if(graph_find_vertex(graph,3)==-1){
    printf("\nOK\n\n");
  }
  else{
    printf("\nECHEC\n\n");
  }

  printf("vérification du cas ou le sommet appartient au graph: validation du test:");

  if(graph_find_vertex(graph,1)==1){
    printf("\nOK\n\n");
  }
  else{
    printf("\nECHEC\n\n");
  }

  //suppression de tous les éléments du graph
  graph_del(graph);

  return(EXIT_SUCCESS);

}
