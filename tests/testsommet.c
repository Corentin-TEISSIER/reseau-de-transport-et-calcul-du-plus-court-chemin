#include "sommet.h"

int main(){

  //création du sommet 0
  //1.création et remplissage d'un sommet
  //2.remplissage de s0.edges

  //1
  vertex_t s0;
  s0.numero=0;
  s0.nom="Fred";
  s0.ligne="A";
  s0.x=0;
  s0.y=0;
  s0.edges=NULL;
  s0.pcc=0;
  //2
  s0.edges=listedge_add_last(arc_new(1,5),s0.edges);
  s0.edges=listedge_add_last(arc_new(2,10),s0.edges);
  s0.edges=listedge_add_last(arc_new(3,15),s0.edges);
  s0.edges=listedge_add_last(arc_new(4,20),s0.edges);


  //création du sommet 1
  vertex_t s1;
  s1.numero=1;
  s1.nom="George";
  s1.ligne="B";
  s1.x=10;
  s1.y=0;
  s1.edges=NULL;
  s1.pcc=0;

  s1.edges=listedge_add_last(arc_new(0,5),s1.edges);
  s1.edges=listedge_add_last(arc_new(2,3),s1.edges);
  s1.edges=listedge_add_last(arc_new(4,15),s1.edges);

  //création du sommet 2
  vertex_t s2;
  s2.numero=2;
  s2.nom="Albert";
  s2.ligne="C";
  s2.x=54;
  s2.y=14;
  s2.edges=NULL;
  s2.pcc=0;

  //création du sommet 3
  vertex_t s3;
  s3.numero=3;
  s3.nom="Josianne";
  s3.ligne="D";
  s3.x=0;
  s3.y=32;
  s3.edges=NULL;
  s3.pcc=0;

  //création du sommet 4
  vertex_t s4;
  s4.numero=4;
  s4.nom="Christine";
  s4.ligne="E";
  s4.x=27;
  s4.y=63;
  s4.edges=NULL;
  s4.pcc=0;


  //affichage du sommet 1:
  vertex_print(s0);
  printf("\n\n");

  //affichage du sommet 2:
  vertex_print(s1);
  printf("\n\n");

  //distance heuristique entre deux sommets:
  printf("distance heuristique entre les sommets numéros 1 et 2: %lf\n\n",vertex_heuristique(s0,s1));


  //accessibilité d'un sommet depuis un autre sommets et récupération du cout (tjrs entre deux sommets voisins)
  printf("peut-on acceder à s0 depuis s0?");
  if(vertex_access(s0,s0)){
    printf("  OUI\ncoût: %lf\n\n",vertex_cost(s0,s0));
  }
  else{
    printf("  NON\n\n");
  }

  printf("peut-on acceder à s1 depuis s0?");
  if(vertex_access(s0,s1)){
    printf("  OUI\ncoût: %lf\n\n",vertex_cost(s0,s1));
  }
  else{
    printf("  NON\n\n");
  }

  printf("peut-on acceder à s2 depuis s0?");
  if(vertex_access(s0,s2)){
    printf("  OUI\ncoût: %lf\n\n",vertex_cost(s0,s2));
  }
  else{
    printf("  NON\n\n");
  }

  printf("peut-on acceder à s3 depuis s0?");
  if(vertex_access(s0,s3)){
    printf("  OUI\ncoût: %lf\n\n",vertex_cost(s0,s3));
  }
  else{
    printf("  NON\n\n");
  }

  printf("peut-on acceder à s4 depuis s0?");
  if(vertex_access(s0,s4)){
    printf("  OUI\ncoût: %lf\n\n",vertex_cost(s0,s4));
  }
  else{
    printf("  NON\n\n");
  }

  printf("peut-on acceder à s0 depuis s1?");
  if(vertex_access(s1,s0)){
    printf("  OUI\ncoût: %lf\n\n",vertex_cost(s1,s0));
  }
  else{
    printf("  NON\n\n");
  }

  printf("peut-on acceder à s1 depuis s1?");
  if(vertex_access(s1,s1)){
    printf("  OUI\ncoût: %lf\n\n",vertex_cost(s1,s1));
  }
  else{
    printf("  NON\n\n");
  }

  printf("peut-on acceder à s2 depuis s1?");
  if(vertex_access(s1,s2)){
    printf("  OUI\ncoût: %lf\n\n",vertex_cost(s1,s2));
  }
  else{
    printf("  NON\n\n");
  }

  printf("peut-on acceder à s3 depuis s1?");
  if(vertex_access(s1,s3)){
    printf("  OUI\ncoût: %lf\n\n",vertex_cost(s1,s3));
  }
  else{
    printf("  NON\n\n");
  }

  printf("peut-on acceder à s4 depuis s1?");
  if(vertex_access(s1,s4)){
    printf("  OUI\ncoût: %lf\n\n",vertex_cost(s1,s4));
  }
  else{
    printf("  NON\n\n");
  }

  printf("\n\n");
  return(EXIT_SUCCESS);
}
