#include <stdio.h>
#include <stdlib.h>
#include "list_vertex.h"

int main() {

  //création de sommets à ajouter au graph
  vertex_t s0;
  s0.numero=0;
  s0.nom="Fred";
  s0.ligne="A";
  s0.x=0;
  s0.y=0;
  s0.edges=NULL;
  s0.pcc=0;
  s0.cout=5;
  edge_t e1=arc_new(1,5);
  s0=vertex_add_edge(s0,e1);
  edge_t e2=arc_new(2,10);
  s0=vertex_add_edge(s0,e2);
  edge_t e3=arc_new(3,15);
  s0=vertex_add_edge(s0,e3);
  edge_t e4=arc_new(4,20);
  s0=vertex_add_edge(s0,e4);

  vertex_t s1;
  s1.numero=1;
  s1.nom="George";
  s1.ligne="B";
  s1.x=10;
  s1.y=0;
  s1.edges=NULL;
  s1.pcc=0;
  s1.cout=20;
  edge_t e5=arc_new(0,5);
  s1=vertex_add_edge(s1,e5);
  edge_t e6=arc_new(2,3);
  s1=vertex_add_edge(s1,e6);
  edge_t e7=arc_new(4,15);
  s1=vertex_add_edge(s1,e7);

  vertex_t s2;
  s2.numero=2;
  s2.nom="Jean";
  s2.ligne="C";
  s2.x=15;
  s2.y=3;
  s2.edges=NULL;
  s2.pcc=0;
  s2.cout=40;
  edge_t e8=arc_new(0,5);
  s2=vertex_add_edge(s2,e8);
  edge_t e9=arc_new(2,3);
  s2=vertex_add_edge(s2,e9);
  edge_t e10=arc_new(4,15);
  s2=vertex_add_edge(s2,e10);


  //ajout des pointeurs sur les sommets

  vertex_t* ps0=&s0;
  vertex_t* ps1=&s1;
  vertex_t* ps2=&s2;

  list_vertex_t l=list_vertex_new();

  //test des ajout de sommets dans la liste
  l=list_vertex_add_first(ps0,l);
  l=list_vertex_add_last(ps1,l);
  l=list_vertex_add_last(ps2,l);
  list_vertex_print(l);

  //test de list_vertex_is_empty
  printf("Vérifions que la liste n'est pas vide: ");
  if(!list_vertex_is_empty(l)){
    printf("c'est bon\n");
  }
  else{
    printf("ERROR\n");
  }

  //test de la recherche du sommet de plus petit cout
  printf("Vérifions que le sommet de plus petit cout est bien le sommet 0:\n");

  int n;
  vertex_t* v;

  int* somnumero=NULL;
  n=list_vertex_find_less_cost(l,somnumero);
  printf("indice de l'élément de plus petit cout: %d\n\n",n);

  v=list_vertex_find_vertex_n(n,l);

  vertex_print(*v); 

  l=list_vertex_remove_n(n,l);
  printf("vérif suppression\n");
  list_vertex_print(l);
  list_vertex_delete(l);

  return(EXIT_SUCCESS);
}
