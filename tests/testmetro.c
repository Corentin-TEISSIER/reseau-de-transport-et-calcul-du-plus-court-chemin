#include "metro.h"
#include "chemin_inverse.h"
#include <time.h>

int main(int argc, char *argv[]){

  if ( argc < 2 ) {
    fprintf( stderr, "Usage %s fichier_graphe\n", argv[0] );
    exit( EXIT_FAILURE );
  }

  graph_t graph = readgraph(argv[1],&graph);

  graph_print(graph);

  int verif;
  int numdepart;
  int numarrivee;
  unsigned long cl;
  cl=clock();
  verif=metro(graph,&numdepart,&numarrivee);
  cl = clock()-cl;

  printf("===========================================\n");
  printf("La fonction metro fonctionne correctement: ");


  if(verif==1){

    printf(" OUI\n\n");
  }
  else{
    printf(" NON\n\n");
    graph_del(graph);
    return(EXIT_FAILURE);
  }

  getchar();

  list_vertex_t listpere=list_vertex_new();


  listpere=list_vertex_add_first(&graph.data[numarrivee],listpere);
  listpere=chemin_inverse(graph, numdepart, numarrivee, &listpere);
  listpere=chemin_inverse_del_double_arrivee(listpere);

  printf("===========================================\n");
  printf("Valeur du plus court chemin:");
  list_vertex_t pointeur=list_vertex_new();
  pointeur=listpere;
  while(!list_vertex_is_empty(pointeur->next)){
    pointeur=pointeur->next;
  }
  double cout;
  cout=(pointeur->val->cout)
  -(listpere->val->cout);
  printf("%lf\n\n", cout);
  getchar();


  printf("===========================================\n");
  printf("AFFICHAGE DU CHEMIN\n");

  list_vertex_line_print(listpere);

  printf("===========================================\n");
  printf("Temps mesure en secondes: %lf\n",cl/(double)CLOCKS_PER_SEC);

  getchar();

  printf("===========================================\n");
  printf("FIN DU TEST\n\n");
  printf("===========================================\n\n\n");

  listpere=list_vertex_delete(listpere);
  graph_del(graph);

  return(EXIT_SUCCESS);
}
