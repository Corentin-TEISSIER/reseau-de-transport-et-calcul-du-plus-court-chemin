#include "arc.h"


int main(){
  edge_t arc1;
  edge_t arc2;
  edge_t arc3;


  arc1 = arc_new(1,5);
  arc2 = arc_new(2,6);
  arc3 = arc_new(3,1);

  arc_print(arc1);
  arc_print(arc2);
  arc_print(arc3);
}
