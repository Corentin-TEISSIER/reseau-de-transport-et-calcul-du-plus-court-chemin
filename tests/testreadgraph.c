#include <stdio.h>
#include <stdlib.h>
#include "readgraph.h"


int main( int argc, char *argv[] ) {
  graph_t graph;

  if ( argc < 2 ) {

    fprintf( stderr, "Usage %s ficher_graphe\n", argv[0] );
    exit( EXIT_FAILURE );
  }

  //lecture du fichier et remplissage du graph
  graph=readgraph(argv[1]);
  //affichage pour vérification
  graph_print(graph);
  graph_del(graph);
  return(EXIT_SUCCESS);
}
