#include "Astar_tas.h"
#include "Astar_list.h"
#include "Dijkstra_list.h"
#include "Dijkstra_tas.h"
#include <time.h>

int main(int argc, char *argv[]){

  if ( argc < 3 ) {
    fprintf( stderr, "Usage %s fichier_graphe nombre_de_données\n", argv[0] );
    exit( EXIT_FAILURE );
  }

  int choix=0;

  while((choix!=1) && (choix!=2) && (choix!=3) && (choix!=4)){
    printf("Choix de l'algorithme:\n");
    printf("Tapez 1 : Dijkstra avec gestion par liste de sommets\n");
    printf("Tapez 2 : Dijkstra avec gestion par tas de sommets\n");
    printf("Tapez 3 : Astar avec gestion par liste de sommets\n");
    printf("Tapez 4 : Astar avec gestion par tas de sommets\n\n");
    scanf("%d",&choix);
    if((choix!=1) && (choix!=2) && (choix!=3) && (choix!=4)){printf("Saisie non valide\n");}
  }

  int n=atoi(argv[2]);
  int i;
  double moy=0;

  unsigned long cl;
  cl=clock();
  graph_t graph = readgraph(argv[1],&graph);
  cl = clock()-cl;
  moy += cl/(double)CLOCKS_PER_SEC;

  printf("===================================\n");
  printf("\nFréquence d'horloge de la machine à titre indicatif: ");
  printf("CLOCK_PER_SEC: %ld\n\n",CLOCKS_PER_SEC);
  printf("===================================\n\n");
  printf("===================================\n\n");
  printf("le temps de création du graphe est : %lf\n\n", moy);
  printf("===================================\n\n");

  moy=0;

  for(i=0;i<n;i++){
    int depart = random()%graph.size_vertices;
    int arrivee = random()%graph.size_vertices;
    cl=clock();
    switch(choix){
      case 1:
        Dijkstra(depart,arrivee,graph);
        break;
      case 2:
        Dijkstra_tas(depart,arrivee,graph);
        break;
      case 3:
        Astar(depart,arrivee,graph);
        break;
      case 4:
        Astar_tas(depart,arrivee,graph);
        break;
    }
    cl = clock()-cl;
    moy += cl/(double)CLOCKS_PER_SEC;

  }

    moy=moy/n;

    printf("===================================\n\n");
    printf("le temps de calcul moyen pour Astar sur ce test est : %lf\n\n", moy);
    printf("===================================\n\n");
    graph_del(graph);
    return(EXIT_SUCCESS);
}
