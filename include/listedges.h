#ifndef _LIST_ARCS_H
#define _LIST_ARCS_H
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "arc.h"


typedef struct maillon_edge {
  edge_t val;
  struct maillon_edge * next;
}* listedge_t;

listedge_t listedge_new(); /* création d'une liste d'arc vide */

int listedge_is_empty(listedge_t l); /* Retourne VRAI si l est une liste vide */

void listedge_print(listedge_t l); /* affiche la liste */

listedge_t listedge_find(int numerosommet, listedge_t l); /* repère si un des arcs appartenant à la liste l à pour arrivée le sommet "sommet", si oui on retourne un pointeur sur ce maillon, sinon on retourne NULL */

listedge_t listedge_find_double(int numerosommet, double cout, listedge_t l);

int listedge_length(listedge_t l); /*Retourne le nombre d'éléments (ou de maillons) de la liste*/

listedge_t listedge_del_first(listedge_t l); /* Supprime le maillon en tête de liste et retourne la nouvelle liste  PRECONDITION : liste non vide */

listedge_t listedge_add_first(edge_t e, listedge_t l); /* Ajoute l'arc e en tête de la liste et retourne la nouvelle liste*/

listedge_t listedge_add_last(edge_t e, listedge_t l) ; /* Ajoute l'arc e en fin de liste */

listedge_t listedge_copy(listedge_t l); /* retourne une copie de la liste l */

listedge_t listedge_delete(listedge_t l); /* Libère toute la liste et retourne une liste vide */

#endif
