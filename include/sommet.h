#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "listedges.h"

#ifndef _VERTEX
#define _VERTEX

/* Definition des sommets */
typedef struct sommet{
  int numero;
  char* nom;
  char* ligne;
  double x;
  double y;
  listedge_t edges;
  double pcc;
  double cout;
  int pere;
  } vertex_t;

//affichage de la composition d'un sommet
void vertex_print(vertex_t v);
//calcul de la distance heuristique entre deux sommets
double vertex_heuristique(vertex_t v1,vertex_t v2);
//vérification de l'existence d'un arc direct de v1 vers v2 (return 1 si TRUE, 0 si FALSE)
int vertex_access(vertex_t v1,vertex_t v2);
//calcul du cout du passage entre deux sommets voisins (de v1 vers v2)
double vertex_cost(vertex_t v1,vertex_t v2);
//ajout d'un arc au sommet;
vertex_t vertex_add_edge(vertex_t sommet, edge_t arc);
//comparaison de deux sommets (considérés égales si ils ont les mêmes numéro, nom, ligne, coordonnées)
int vertex_equal(vertex_t* v1, vertex_t* v2);

#endif
