#ifndef _METRO_H
#define _METRO_H

#include <stdio.h>
#include <stdlib.h>
#include "readgraph.h"
#include "graph.h"
#include "sommet.h"

int metro(graph_t graphe,int* numdepart,int* numarrivee);

//Ce programme étant fait uniquement pour tester la capacité à trouver le chemin en récupérant le nom d'une destinations
//nous n'utiliseront que l'algorythme Astar avec des tas pour éviter d'avoir trop de programme dans le répertoir


#endif
