#ifndef _ASTAR_TAS_H
#define _ASTAR_TAS_H

#include <stdio.h>
#include <stdlib.h>
#include "readgraph.h"
#include "graph.h"
#include "sommet.h"

int Astar_tas(int depart, int arrivee, graph_t graphe);

#endif
