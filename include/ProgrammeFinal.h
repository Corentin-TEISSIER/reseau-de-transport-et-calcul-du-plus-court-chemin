#ifndef _PROGRAMMEFINAL_H
#define _PROGRAMMEFINAL_H

#include <stdio.h>
#include <stdlib.h>
#include "readgraph.h"
#include "graph.h"
#include "sommet.h"
#include "Dijkstra_tas.h"
#include "Astar_tas.h"
#include "Dijkstra_list.h"
#include "Astar_list.h"
#include "tas_vertex.h"
#include "list_vertex.h"
#include "chemin_inverse.h"
#include <time.h>
#include "metro.h"

int ProgrammeFinal();
int choix_fichier();

#endif
