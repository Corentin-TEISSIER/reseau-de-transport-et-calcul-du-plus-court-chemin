#ifndef _DIJKSTRA_TAS_H
#define _DIJKSTRA_TAS_H
#include <stdio.h>
#include <stdlib.h>
#include "readgraph.h"

int Dijkstra_tas(int depart, int arrivee, graph_t graphe);

#endif
