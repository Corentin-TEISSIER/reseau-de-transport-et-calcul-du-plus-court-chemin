#ifndef _ASTAR_LIST_H
#define _ASTAR_LIST_H

#include <stdio.h>
#include <stdlib.h>
#include "readgraph.h"
#include "graph.h"

#include "sommet.h"

int Astar(int depart, int arrivee, graph_t graphe);

#endif
