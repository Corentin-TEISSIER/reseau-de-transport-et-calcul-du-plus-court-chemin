#ifndef _DIJKSTRA_LIST_H
#define _DIJKSTRA_LIST_H
#include <stdio.h>
#include <stdlib.h>
#include "readgraph.h"

int Dijkstra(int depart, int arrivee, graph_t graphe);

#endif
