#include <stdio.h>
#include <stdlib.h>
#include "sommet.h"
 
#ifndef _GRAPH
#define _GRAPH

/* Definition des graph */
typedef struct {
  int nverts;
  int size_vertices;
  int size_edges;
  vertex_t* data;
  } graph_t;

//initialisation d'un graph vide
graph_t graph_init(int size_v, int size_e);
//ajout d'un sommet au graph: retourne le graph avec le sommet supplémentaire si le nombre de sommet total du graph ne dépasse pas grap.size_vertices
graph_t graph_add_vertex(graph_t graph, vertex_t s);
//affichage du graph_t
void graph_print(graph_t graph);
//destruction du graph et libération mémoire
void graph_del(graph_t graph);
//trouver l'indice du sommet "sommet" dans le graph "graph"
int graph_find_vertex(graph_t graph,int numero);


#endif
