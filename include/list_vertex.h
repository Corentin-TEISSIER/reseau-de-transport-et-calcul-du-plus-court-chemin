#ifndef _LIST_VERTEX_H
#define _LIST_VERTEX_H
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "sommet.h"

typedef struct _link {
  vertex_t* val;
  struct _link *next;
} link_t, *list_vertex_t;


// Crée une list_vertex vide
list_vertex_t list_vertex_new() ;
// Retourne VRAI si l est une list_vertex vide
int list_vertex_is_empty(list_vertex_t l);
// Retourne l'élément en tête de list_vertex
// PRECONDITION : list_vertex non vide
vertex_t* list_vertex_first(list_vertex_t l);
// Retourne le reste de la list_vertex
// PRECONDITION : list_vertex non vide
list_vertex_t list_vertex_next(list_vertex_t l);
// Ajoute l'élément e en tête de la list_vertex et retourne la nouvelle list_vertex
list_vertex_t list_vertex_add_first(vertex_t* e, list_vertex_t l);
// Supprime le maillon en tête de list_vertex et retourne la nouvelle list_vertex
// PRECONDITION : list_vertex non vide
list_vertex_t list_vertex_del_first(list_vertex_t l);
//Supprime le maillon en queue de liste_vertex et retourne la nouvelle list_vertex
list_vertex_t list_vertex_del_last(list_vertex_t l);
// Retourne le nombre d'éléments (ou de maillons) de la list_vertex
int list_vertex_length(list_vertex_t l);
// Retourne un pointeur sur le premier maillon contenant e,
// ou NULL si e n'est pas dans la list_vertex
list_vertex_t list_vertex_find(int num, list_vertex_t l);
// Affiche la list_vertex
void list_vertex_print(list_vertex_t l);
//Affiche la liste et les correspondances
void list_vertex_line_print(list_vertex_t l);
// Libère toute la list_vertex et retourne une list_vertex vide
list_vertex_t list_vertex_delete(list_vertex_t l);
// Compte le nombre de e dans la list_vertex
int list_vertex_count(vertex_t e, list_vertex_t l);
// Ajoute en fin de list_vertex
list_vertex_t list_vertex_add_last(vertex_t* e, list_vertex_t l);
// Concatene 2 list_vertexs
list_vertex_t list_vertex_concat(list_vertex_t l1, list_vertex_t l2);
// Clone une list_vertex
list_vertex_t list_vertex_copy(list_vertex_t l);
// Supprime l'element en position n et retourne la nouvelle list_vertex
list_vertex_t list_vertex_remove_n(int n, list_vertex_t l);
// Retourner la place du sommet de plus faible cout dans l
int list_vertex_find_less_cost(list_vertex_t l, int* numero);
//retourne le sommet en n-ième position de la liste l
vertex_t* list_vertex_find_vertex_n(int n, list_vertex_t l);



#endif
