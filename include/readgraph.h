#ifndef _READ_GRAPH_H
#define _READ_GRAPH_H
#include <stdio.h>
#include <stdlib.h>
#include "graph.h"

graph_t readgraph(char* nom,graph_t* graph);

#endif
