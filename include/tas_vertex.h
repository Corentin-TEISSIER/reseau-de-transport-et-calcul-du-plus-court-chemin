#ifndef _TAS_VERTEX
#define _TAS_VERTEX

#include <stdio.h>
#include <stdlib.h>
#include "sommet.h"

typedef
  struct {
    vertex_t** data;
    int max_size;
    int number;
  } tas_vertex_t;

//crée un arbre vide et alloue la memoire pour m vertex_t*
tas_vertex_t tas_vertex_new(int m);
//vérifie si l'arbre est vide ou non
int tas_vertex_is_empty(tas_vertex_t tas);
//ajoute un vertex_t* à la dernière position et remonte ce vertex_t* en bonne position suivant le cout
tas_vertex_t tas_vertex_add(vertex_t* valeur, tas_vertex_t* ptas);
//retourne le premier élément du tas
vertex_t* tas_vertex_get_min(tas_vertex_t tas);
//supprime le premier vertex_t* et réorganise le tas
int tas_vertex_delete_min(tas_vertex_t* ptas);
//int tas_vertex_verification(tas_vertex_t tas) ;
int tas_vertex_smallest_son(tas_vertex_t tas, int indice);
//free l'ensemble de l'espace alloué au tas
void tas_vertex_delete(tas_vertex_t tas);
//affiche le tas
void tas_vertex_print(tas_vertex_t tas) ;
//vérifier qu'un élément appartient au tas_vertex
int tas_vertex_find(vertex_t* v,tas_vertex_t tas);

// #define TAS_VERTEX_FATHER(i) ( ((i)-1)/2)
// #define TAS_VERTEX_LEFTSON(i) ( 2*(i)+1)
// #define TAS_VERTEX_RIGHTSON(i) ( 2*((i)+1))



#endif
