#ifndef _CHEMIN_INVERSE
#define _CHEMIN_INVERSE

#include "graph.h"
#include "list_vertex.h"

list_vertex_t chemin_inverse(graph_t graph, int depart, int arrivee, list_vertex_t* listpere);
list_vertex_t chemin_inverse_del_double_arrivee(list_vertex_t listpere);
#endif
