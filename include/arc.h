#ifndef _ARC_H
#define _ARC_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


typedef struct {
  int arrivee; /* indice du sommet d'arrivée de l'arc*/
  double cout; /* coût : distance de l'arc */
} edge_t;

edge_t arc_new(int arrivee, double cout); /*création d'un arc d'arrivee "arrivee" et de cout "cout"*/
void arc_print(edge_t arc); /* affiche l'indice d'arrivée de l'arc et son coût*/
int arc_compare(edge_t arc1,edge_t arc2);




#endif
