# Ce Makefile permet de générer les executables
# - pour les tests f1main et f2main du repertoire tests
#-  pour le programme pccmain du repertoire src

# les fichiezrs executables sont situés sdnas le repertoire bin


#Les repertoires
#Pour les fichiers d'entete
INCDIR=./include
#Pour les fichiers executables
BINDIR=./bin
#Pour les fichiers binaires (.o)
OBJDIR=./obj
#Pour les fichiers de tests
TESTS=./tests
#Pour les fichiers sources .c
SRCDIR=./src

#Le nom du compilateur
CC=gcc

#Les options du compilateur : compilation (-c) et debug (-g). On peut ajouter -O3 pour optimiser quand le code est juste
CFLAGS=-c -Wall -g -I$(INCDIR)

#Les options de l'editeur de liens : -lm pour la bibliothèque mathématique. Voir les Makefile de TP pour ajouter la SDL si besoin
LDFLAGS= -lm

#Les executables que l'on veut construire: a la fois ceux des tests et ceux des programmes finaux
EXEDIR=$(BINDIR)/pccmain $(BINDIR)/f1main $(BINDIR)/f2main $(BINDIR)/testarc $(BINDIR)/testlistedges


#Les fichiers binaire : ajouter les noms des nouveaux fichiers ici
OBJ=$(OBJDIR)/f1.o $(OBJDIR)/f2.o $(OBJDIR)/arc.o $(OBJDIR)/listedges.o

#Pour construire tous les executables
all: $(EXEDIR)

#pour construire pccmain qui utilise f1.o et f2.o
$(BINDIR)/pccmain : $(OBJ) $(OBJDIR)/pccmain.o
	$(CC) -o $@ $^ $(LDFLAGS)


#pour construire le test f1main qui utilise f1.o
$(BINDIR)/f1main : $(OBJDIR)/f1.o $(OBJDIR)/f1main.o
	$(CC) -o $@ $^ $(LDFLAGS)

#pour construire le test f2main qui utilise f2.o
$(BINDIR)/f2main : $(OBJDIR)/f2.o $(OBJDIR)/f2main.o
	$(CC) -o $@ $^ $(LDFLAGS)

#pour construire le test testarc qui utilise arc.o
$(BINDIR)/testarc : $(OBJDIR)/arc.o $(OBJDIR)/testarc.o
	$(CC) -o $@ $^ $(LDFLAGS)

#pour construire le test testgraph qui utilise graph.o testgraph.o listedges.o sommet.o arc.o
$(BINDIR)/testgraph : $(OBJDIR)/graph.o $(OBJDIR)/testgraph.o $(OBJDIR)/listedges.o $(OBJDIR)/sommet.o $(OBJDIR)/arc.o
	$(CC) -o $@ $^ $(LDFLAGS)

#pour construire le test testsommet qui utilise sommet.o et listedges.o arc.o testsommet.o
$(BINDIR)/testsommet : $(OBJDIR)/sommet.o $(OBJDIR)/listedges.o $(OBJDIR)/arc.o $(OBJDIR)/testsommet.o
	$(CC) -o $@ $^ $(LDFLAGS)

#pour construire le test testlistedges qui utilise listedges.o arc.o testlistedges.o
$(BINDIR)/testlistedges : $(OBJDIR)/listedges.o $(OBJDIR)/arc.o $(OBJDIR)/testlistedges.o
	$(CC) -o $@ $^ $(LDFLAGS)

#pour construire le test testlistvertex qui utilise testlistvertex.o listvertex.o sommet.o listedges.o arc.o
$(BINDIR)/testlistvertex : $(OBJDIR)/list_vertex.o $(OBJDIR)/sommet.o $(OBJDIR)/testlistvertex.o $(OBJDIR)/listedges.o $(OBJDIR)/arc.o
	$(CC) -o $@ $^ $(LDFLAGS)

#construction du test
$(BINDIR)/testtasvertex : $(OBJDIR)/testtasvertex.o $(OBJDIR)/readgraph.o $(OBJDIR)/tas_vertex.o $(OBJDIR)/graph.o $(OBJDIR)/listedges.o $(OBJDIR)/sommet.o $(OBJDIR)/arc.o
	$(CC) -o $@ $^ $(LDFLAGS)

#pour construire le test testreadgraph qui utilise
$(BINDIR)/testreadgraph : $(OBJDIR)/graph.o $(OBJDIR)/readgraph.o $(OBJDIR)/testreadgraph.o $(OBJDIR)/listedges.o $(OBJDIR)/sommet.o $(OBJDIR)/arc.o
	$(CC) -o $@ $^ $(LDFLAGS)

#pour construire le test testDijkstralist qui utilise
$(BINDIR)/testDijkstralist : $(OBJDIR)/testDijkstralist.o $(OBJDIR)/Dijkstra_list.o $(OBJDIR)/chemin_inverse.o $(OBJDIR)/graph.o $(OBJDIR)/readgraph.o $(OBJDIR)/list_vertex.o $(OBJDIR)/listedges.o $(OBJDIR)/sommet.o $(OBJDIR)/arc.o
	$(CC) -o $@ $^ $(LDFLAGS)

#pour construire le test testDijkstratas qui utilise
$(BINDIR)/testDijkstratas : $(OBJDIR)/testDijkstratas.o $(OBJDIR)/Dijkstra_tas.o $(OBJDIR)/chemin_inverse.o $(OBJDIR)/graph.o $(OBJDIR)/readgraph.o $(OBJDIR)/list_vertex.o $(OBJDIR)/tas_vertex.o $(OBJDIR)/listedges.o $(OBJDIR)/sommet.o $(OBJDIR)/arc.o
	$(CC) -o $@ $^ $(LDFLAGS)

#construction du testAstarlist
$(BINDIR)/testAstarlist : $(OBJDIR)/testAstarlist.o $(OBJDIR)/Astar_list.o $(OBJDIR)/chemin_inverse.o $(OBJDIR)/graph.o $(OBJDIR)/readgraph.o $(OBJDIR)/list_vertex.o $(OBJDIR)/listedges.o $(OBJDIR)/sommet.o $(OBJDIR)/arc.o
	$(CC) -o $@ $^ $(LDFLAGS)

#construction du testAstartas
$(BINDIR)/testAstartas : $(OBJDIR)/testAstartas.o $(OBJDIR)/Astar_tas.o $(OBJDIR)/chemin_inverse.o $(OBJDIR)/graph.o $(OBJDIR)/readgraph.o $(OBJDIR)/list_vertex.o $(OBJDIR)/tas_vertex.o $(OBJDIR)/listedges.o $(OBJDIR)/sommet.o $(OBJDIR)/arc.o
	$(CC) -o $@ $^ $(LDFLAGS)

#construction du testmetro
$(BINDIR)/testmetro : $(OBJDIR)/testmetro.o $(OBJDIR)/metro.o $(OBJDIR)/chemin_inverse.o $(OBJDIR)/graph.o $(OBJDIR)/readgraph.o $(OBJDIR)/list_vertex.o $(OBJDIR)/tas_vertex.o $(OBJDIR)/listedges.o $(OBJDIR)/sommet.o $(OBJDIR)/arc.o
	$(CC) -o $@ $^ $(LDFLAGS)

#construction du temps de calcul moyen pour Astar_tas
$(BINDIR)/moyennetemps : $(OBJDIR)/moyennetemps.o $(OBJDIR)/Astar_tas.o $(OBJDIR)/Astar_list.o $(OBJDIR)/Dijkstra_tas.o $(OBJDIR)/Dijkstra_list.o $(OBJDIR)/graph.o $(OBJDIR)/readgraph.o $(OBJDIR)/list_vertex.o $(OBJDIR)/tas_vertex.o $(OBJDIR)/listedges.o $(OBJDIR)/sommet.o $(OBJDIR)/arc.o
	$(CC) -o $@ $^ $(LDFLAGS)

#construction du programme final
$(BINDIR)/testProgrammeFinal : $(OBJDIR)/testProgrammeFinal.o $(OBJDIR)/ProgrammeFinal.o $(OBJDIR)/metro.o $(OBJDIR)/chemin_inverse.o $(OBJDIR)/Astar_tas.o $(OBJDIR)/Astar_list.o $(OBJDIR)/Dijkstra_tas.o $(OBJDIR)/Dijkstra_list.o $(OBJDIR)/graph.o $(OBJDIR)/readgraph.o $(OBJDIR)/list_vertex.o $(OBJDIR)/tas_vertex.o $(OBJDIR)/listedges.o $(OBJDIR)/sommet.o $(OBJDIR)/arc.o
	$(CC) -o $@ $^ $(LDFLAGS)


# pour construire les fichiers binaires .o
$(OBJDIR)/%.o : $(TESTS)/%.c
	$(CC) $(CFLAGS) $^ -o $@

$(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $^ -o $@


clean:
	rm -rf $(OBJDIR)/* $(BINDIR)/* $(EXEDIR) *.dSYM
